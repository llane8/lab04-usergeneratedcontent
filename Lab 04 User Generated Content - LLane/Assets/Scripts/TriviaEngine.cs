﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/// <summary>
/// TriviaEngine.cs is responsible for setting up the game, responding to player input,
/// and deciding if the game is won or lost.
/// @author Linda Lane 
/// @date 9/24/2016
/// // Code based on model provided by T. Fisher, Instructor, GAME 221
/// </summary>

//Force unity to add the references script if it is not already on the object
[RequireComponent(typeof(TriviaReferences))]
public class TriviaEngine : MonoBehaviour
{

    //Variables
    TriviaReferences references;
    string question = " ";              //Stores the trivia question
    string rightAnswer = " ";           //Stores the right answer
    string wrongAnswer1 = " ";           //Stores the wrong answer1
    string wrongAnswer2 = " ";           //Stores the wrong answer2
    string wrongAnswer3 = " ";           //Stores the wrong answer3

    public int totalRight = 0;                           //Stores how many answers the player got right
    public int totalWrong = 0;                          //Stores how many answers the player got wrong
    public int randomSent;


    /// <summary>
    /// Called by TriviaReferences once it is done gathering all neccessary information
    /// Starts the game
    /// </summary>
    void Begin()
    {
        //Gets a link to the references script
        references = gameObject.GetComponent<TriviaReferences>();
        //Starts the game
        StartGame();
    }

    /// <summary>
    /// Called by buttons in the scene to check the players input.
    /// </summary>
    /// <param name="answer">The number of the answer the player guessed </param>
    /// 
   public void CheckAnswer(string answer)
    {
        //Find the button the player clicked and disable it so it cannot be clicked again
        GameObject.Find(answer).GetComponent<Button>().interactable = false;

        Debug.Log("Answer = " + answer);

  /*    if (answer == references.rightAnswers[])
        {
            totalRight++;
        }
        else
        {
            totalWrong++;
        } 
*/
     
    /* 
        //If the player has no points left, end the game
        if ((totalRight+totalWrong) <= ran)
            EndGame();

      
         // WinGame();

 */

     } 

    /// <summary>
    /// Shows a win image, a short text informing the player they won, and a restart button
    /// </summary>
    void WinGame()
    {
        references.gameWonImage.enabled = true;
        references.gameWonText.enabled = true;
        references.quitFromGWPanel.SetActive(true);
        references.restartFromGWPanel.SetActive(true);

    }

    /// <summary>
    /// Shows the player the answer, an image, a short text saying they lost, and a restart button
    /// </summary>
    void EndGame()
    {
        references.gameOverImage.enabled = true;
        references.gameOverText.enabled = true;
        references.quitFromGOPanel.SetActive(true);
        references.restartFromGOPanel.SetActive(true);
    }

    /// <summary>
    /// Sets all player data to default values 
    /// </summary>
    public void StartGame()
    {
        //Sets the goal, and how many letters the player has guessed to zero
        totalRight = 0;
        totalWrong = 0;

        // get the number of questions that are in the imported file
        int numberOfQuestionsSent = references.questions.Count;

        // randomly select one question
        randomSent = Random.Range(0, numberOfQuestionsSent);

            // Do this repetitively until GameOver or game is won
        question = references.questions[randomSent];
        rightAnswer = references.rightAnswers[randomSent];
        wrongAnswer1 = references.wrongAnswers[randomSent * 3];
        wrongAnswer2 = references.wrongAnswers[(randomSent * 3) + 1];
        wrongAnswer3 = references.wrongAnswers[(randomSent * 3) + 2];


        //Disable all game over/won images, texts, and buttons
        references.gameOverImage.enabled = false;
        references.gameOverText.enabled = false;
        references.quitFromGOPanel.SetActive(false);
        references.restartFromGOPanel.SetActive(false);
        references.gameWonImage.enabled = false;
        references.gameWonText.enabled = false;
        references.quitFromGWPanel.SetActive(false);
        references.restartFromGWPanel.SetActive(false);


        // show the question and the answers
        references.questionText.text = question;
        references.answerButton1Text.text = rightAnswer;
        references.answerButton2Text.text = wrongAnswer1;
        references.answerButton3Text.text = wrongAnswer2;
        references.answerButton4Text.text = wrongAnswer3;

        // show the beginning number of right and wrong questions
        references.totalRight.text = "Right" + totalRight;
        references.totalWrong.text = "Wrong" + totalWrong;

        //Enable all of the buttons on the screen so the player can interact again
        foreach (Button btn in references.answerButtons)
        {
            btn.interactable = true;
        }
    }


}
