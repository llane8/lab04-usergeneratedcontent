﻿// Lab 04 - User Generated Content
// JCCC GAME 221 Fall 2016
//@ author Linda Lane
//@ date 9/24/2016

using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

[RequireComponent(typeof(TriviaReferences))]
public class TriviaLoadScript : MonoBehaviour
{
    FileInfo gameMastersFile;
    TextAsset gameStarterFile;
    TextReader reader;

    public List<string> questions = new List<string>();
    public List<string> rightAnswers = new List<string>();
    public List<string> wrongAnswers = new List<string>();

    void Start()
    {
        gameMastersFile = new FileInfo(Application.dataPath + "/GameMastersTrivia.txt");

        if (gameMastersFile != null && gameMastersFile.Exists)
        {
            reader = gameMastersFile.OpenText();
        }
        else
        {

            gameStarterFile = (TextAsset)Resources.Load("GameStartTrivia", typeof(TextAsset));
            reader = new StringReader(gameStarterFile.text);
        }

        string lineOfText;
        int lineNumber = 0;

        //tell the reader to read a line of text, and store that in the lineOfTextVariable
        //continue doing this until there are no lines left
        // the first line of each 5 lines is the question
        // the second line of each 5 lines is the correct answer
        while ((lineOfText = reader.ReadLine()) != null)
        {
            if (lineNumber % 5 == 0)
            {
                questions.Add(lineOfText);
            }
            else if (lineNumber % 5 == 1 )
            {
                rightAnswers.Add(lineOfText);
            }
           else
            {
                wrongAnswers.Add(lineOfText);
            }
     

            lineNumber++;
        }

        SendMessage("Gather");
    }

}
