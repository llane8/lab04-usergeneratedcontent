﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
/// <summary>
/// TriviaReferences.cs gathers references from various objects in the screen.
/// It then stores the references for use in the game at later points. 
/// @author Linda Lane 9/24/2016
/// Based on code proveded for Lab in GAME 221 by Professor T. Fisher
/// </summary>

public class TriviaReferences : MonoBehaviour
{

    //Variables to store all information, hidden from designer
    [HideInInspector]
    public Text totalRight;
    [HideInInspector]
    public Text totalWrong;

    [HideInInspector]
    public GameObject Answer1_Button, Answer2_Button, Answer3_Button, Answer4_Button;
    [HideInInspector]
    public GameObject Quit_Button, Restart_Button;
    [HideInInspector]
    public GameObject Quit_Button1, Restart_Button1;
    [HideInInspector]
    public GameObject Panel_GameOver, Panel_Win, Panel_Questions;
    [HideInInspector]
    public GameObject Panel_Right_Count, Panel_Wrong_Count;

    [HideInInspector]
    public Image gameOverImage, gameWonImage;
    [HideInInspector]
    public Text gameOverText, gameWonText;
    [HideInInspector]
    public GameObject quitFromGOPanel, restartFromGOPanel, quitFromGWPanel, restartFromGWPanel;

    [HideInInspector]
    public Text questionText;
    [HideInInspector]
    public Text answerButton1Text;
    [HideInInspector]
    public Text answerButton2Text;
    [HideInInspector]
    public Text answerButton3Text;
    [HideInInspector]
    public Text answerButton4Text;

    [HideInInspector]
    public Button[] answerButtons;

    [HideInInspector]
    public List<string> questions = new List<string>();
    [HideInInspector]
    public List<string> rightAnswers = new List<string>();
    [HideInInspector]
    public List<string> wrongAnswers = new List<string>();


    // Gather replaces the Start() function and is called as soon as the TriviaLoadScript calls it after loading data
    void Gather()
    {
        // Gather the references to the question, the right answer and the 3 wrong answers
        questions = GetComponent<TriviaLoadScript>().questions;
        rightAnswers = GetComponent<TriviaLoadScript>().rightAnswers;
        wrongAnswers = GetComponent<TriviaLoadScript>().wrongAnswers;

        questionText = GameObject.Find("QuestionText").GetComponent<Text>();
        answerButton1Text = GameObject.Find("Answer1_Button").GetComponentInChildren<Text>();
        answerButton2Text = GameObject.Find("Answer2_Button").GetComponentInChildren<Text>();
        answerButton3Text = GameObject.Find("Answer3_Button").GetComponentInChildren<Text>();
        answerButton4Text = GameObject.Find("Answer4_Button").GetComponentInChildren<Text>();



        // Gather references to the writeAnswerCount and the wrongAnswerCount text locations
        totalRight = GameObject.Find("Panel_Right_Count").GetComponent<Text>();
        totalWrong = GameObject.Find("Panel_Wrong_Count").GetComponent<Text>();

        // Gather the images, text and buttons for GameOver 
        gameOverImage = GameObject.Find("Panel_GameOver").GetComponent<Image>();
        gameOverText = gameOverImage.gameObject.GetComponentInChildren<Text>();
        quitFromGOPanel = GameObject.Find("Quit_Button");
        restartFromGOPanel = GameObject.Find("Restart_Button");

        // Gather the images, text and buttons for YouWin
        gameWonImage = GameObject.Find("Panel_Win").GetComponent<Image>();
        gameWonText = gameWonImage.gameObject.GetComponentInChildren<Text>();
        quitFromGWPanel = GameObject.Find("Quit_Button1");
        restartFromGWPanel = GameObject.Find("Restart_Button1");

        //Disable the graphics for game over and game won
        //(You cannot use GameObject.Find to locate a disabled object, so they start
        //enabled and are manually disabled)
        gameOverImage.enabled = false;
        gameOverText.enabled = false;
        gameWonImage.enabled = false;
        gameWonText.enabled = false;
        quitFromGOPanel.SetActive(false);
        restartFromGOPanel.SetActive(false);
        quitFromGWPanel.SetActive(false);
        restartFromGWPanel.SetActive(false);


        //Gather references to all of the buttons that correspond with answers
        answerButtons = GameObject.Find("Answers").GetComponentsInChildren<Button>();

        //Inform the Engine to begin the game!
        SendMessage("Begin");
    }


}

